import { useEffect, useState } from "react";
import { View, StyleSheet, Text, ActivityIndicator, Pressable, SafeAreaView, Image, Linking, Platform } from "react-native";
import { BarCodeScanner } from "expo-barcode-scanner";
import * as SecureStore from "expo-secure-store";
import { useNavigation } from "@react-navigation/native";
import { useToast } from "react-native-toast-notifications";

import exitImgSrc from "../../assets/power.png";

const HomeScreen = () => {
    const [hasPermission, setHasPermission] = useState(null);
    const [scanned, setScanned] = useState(false);
    const [text, setText] = useState("Not yet scanned");
    const [isLoading, setIsLoading] = useState(false);

    const navigation = useNavigation();
    const toast = useToast();

    const askForCameraPermission = async () => {
        setHasPermission(null);
        const { status } = await BarCodeScanner.requestPermissionsAsync();
        if (status == "denied" && Platform.OS) Linking.openSettings();
        setHasPermission(status == "granted");
    };

    const handleBarCodeScanned = async ({ type, data }) => {
        setScanned(true);
        setText(data);
        setIsLoading(true);

        const userId = await SecureStore.getItemAsync("userId");

        const response = await (
            await fetch("http://18.117.175.52:4012/api/v1/verifyQrCode", {
                method: "POST",
                headers: {
                    Accept: "application/json",
                    "Content-Type": "application/json",
                },
                body: JSON.stringify({
                    sessionId: data,
                    userId: userId,
                }),
            })
        ).json();

        toast.show(response.message, {
            type: response?.success ? "success" : "danger",
            placement: "top",
            duration: 3000,
            offset: 30,
            animationType: "zoom-in",
        });

        setIsLoading(false);
    };

    const exitHandler = async () => {
        await SecureStore.setItemAsync("userId", "");
        navigation.navigate("Login");
    };

    useEffect(() => {
        askForCameraPermission();
    }, []);

    if (hasPermission === null) {
        return (
            <View style={styles.container}>
                <Text style={styles.text}>Requesting for camera permission</Text>
                <ActivityIndicator size="large" />
            </View>
        );
    }

    if (hasPermission === false) {
        return (
            <View style={styles.container}>
                <Text style={styles.text}>Camera permission is denied</Text>
                <Pressable onPress={askForCameraPermission} style={styles.buttonContainer}>
                    <Text style={styles.button}>Try Again</Text>
                </Pressable>
            </View>
        );
    }

    return (
        <SafeAreaView style={styles.container}>
            <Pressable onPress={exitHandler} style={styles.exitButtonContainer}>
                <Image style={styles.exitIcon} source={exitImgSrc} />
            </Pressable>
            <View style={styles.barcodebox}>
                <BarCodeScanner onBarCodeScanned={scanned ? undefined : handleBarCodeScanned} style={{ height: 400, width: 400 }} />
            </View>
            <Text style={styles.resultText}>{text}</Text>
            <Pressable
                onPress={() => {
                    setScanned(false);
                    setText("Not Yet scanned");
                }}
                style={styles.buttonContainer}>
                {scanned && !isLoading ? <Text style={styles.button}>Scan again</Text> : <ActivityIndicator />}
            </Pressable>
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "#f9fbfc",
        gap: 30,
    },
    text: {
        textAlign: "center",
        fontSize: 25,
        fontWeight: "bold",
    },

    buttonContainer: {
        width: "60%",
        backgroundColor: "rgb(76 29 149)",
        padding: 10,
        marginVertical: 5,
        borderRadius: 5,
        alignItems: "center",
    },
    button: {
        color: "#fff",
        fontWeight: "bold",
    },

    exitButtonContainer: {
        position: "absolute",
        borderColor: "#000",
        borderWidth: 2,
        top: 50,
        right: 20,
        height: 40,
        width: 40,
        justifyContent: "center",
        alignItems: "center",
        borderRadius: "100%",
    },

    exitIcon: { width: "70%", height: "70%" },

    resultText: {
        fontSize: 20,
    },

    barcodebox: {
        backgroundColor: "#fff",
        alignItems: "center",
        justifyContent: "center",
        overflow: "hidden",
        borderRadius: 30,
    },
});
export default HomeScreen;
