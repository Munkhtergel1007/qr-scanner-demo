import { Text, View, StyleSheet, TextInput, Pressable, SafeAreaView, ActivityIndicator } from "react-native";
import { useState } from "react";
import { useNavigation } from "@react-navigation/native";
import * as SecureStore from "expo-secure-store";
import { useToast } from "react-native-toast-notifications";

const LoginScreen = () => {
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [isLoading, setLoading] = useState(false);
    const navigation = useNavigation();
    const toast = useToast();

    const LoginHandler = async () => {
        if (username.trim() === "" || password.trim() === "") {
            toast.show("Please enter username or password!", {
                type: "danger",
                placement: "top",
                duration: 3000,
                offset: 30,
                animationType: "slide-in",
            });
            return;
        }

        setLoading(true);

        const response = await (
            await fetch("http://18.117.175.52:4012/api/v1/login", {
                method: "POST",
                headers: {
                    Accept: "application/json",
                    "Content-Type": "application/json",
                },
                body: JSON.stringify({
                    username: username,
                    password: password,
                }),
            })
        ).json();

        setLoading(false);

        if (!response.success) {
            toast.show("Wrong password username!", {
                type: "danger",
                placement: "top",
                duration: 3000,
                offset: 30,
                animationType: "zoom-in",
            });
            return;
        }

        setUsername("");
        setPassword("");
        await SecureStore.setItemAsync("userId", response.result.user?.user_id);

        navigation.navigate("Home");
    };

    return (
        <SafeAreaView style={styles.container}>
            <Text style={styles.title}>LOGO</Text>
            <View style={styles.inputContainer}>
                <TextInput style={styles.input} placeholder="Username" value={username} onChangeText={setUsername} />
            </View>
            <View style={styles.inputContainer}>
                <TextInput style={styles.input} placeholder="Password" value={password} onChangeText={setPassword} secureTextEntry={true} />
            </View>
            <Pressable onPress={LoginHandler} style={styles.buttonContainer}>
                {isLoading ? <ActivityIndicator /> : <Text style={styles.button}>Log In</Text>}
            </Pressable>
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
    },

    title: {
        fontWeight: "bold",
        fontSize: 100,
        paddingVertical: 15,
    },

    inputContainer: {
        backgroundColor: "#fff",
        width: "70%",
        borderColor: "#e8e8e8",
        borderRadius: 5,
        borderWidth: 1,
        padding: 15,
        marginVertical: 5,
    },

    input: {},

    buttonContainer: {
        width: "70%",
        backgroundColor: "rgb(76 29 149)",
        padding: 15,
        marginVertical: 5,
        borderRadius: 5,
        alignItems: "center",
    },
    button: {
        color: "#fff",
        fontWeight: "bold",
    },
});

export default LoginScreen;
